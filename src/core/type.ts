export enum GsapPlugin {
  "SplitText",
  "DrawSVG",
  "ScrambleText",
  "Inertia",
}

export interface MainProps {
  title?: string | undefined;
  description?: string | undefined;
  includeFontAwesome?: boolean | undefined;
  includeGsap?: {
    enabled?: boolean;
    plugins?: GsapPlugin[];
  };
}
